jQuery.noConflict();
(function($) {
    $(function() {
        function initCarousel() {
            $('.carousel').carousel({
                pause: true,
                interval: false
            });
        }

        function countDown() {
            if ($("#clock").length > 0) {
                $('#clock').countdown('2018/3/12', function(event) {
                    var $this = $(this).html(event.strftime('' +
                        '<span class="float-left" ><em>%D</em><label>DIAS</lable></span>  ' +
                        '<span class="float-left" ><em>%H</em><label>HRS</label></span>  ' +
                        '<span class="float-left" ><em>%M</em><label>MIN</label></span>  '
                    ));
                });
            }
        }

        function destacadaHeader() {
            if ($("[data-headerfixed]").length > 0) {
                var $header = $("header");
                $header.addClass("trans");
                $(".navbar-expand-lg").removeClass("navbar-expand-lg");
                $("[data-headerfixed]").insertAfter($(".navbar-brand"));
            }
        }

        function navbarWithPlayer(timeOut) {
            $livePlayer = $(".opening-live:not(.not-logged)");

            if ($livePlayer.length > 1) {
                livePlayerBottom = Math.floor($livePlayer.outerHeight(true) + $livePlayer.offset().top);
                $nav = $('header.main-header > nav')
                $header = $('header.main-header');
                isHidden = false;
                scrolled = false;

                function hideNav() {
                    $nav.fadeOut('250ms');
                    $header.addClass('hidden-nav');
                    setTimeout(function () {
                        isHidden = true;
                    }, 250);
                }

                function showNav() {
                    $header.removeClass('hidden-nav');
                    $nav.fadeIn('250ms');
                    setTimeout(function () {
                        isHidden = false;
                    }, 250);
                }

                $(window).on('scroll', function(event) {
                    var scroll = $(window).scrollTop();
                    if (scroll > livePlayerBottom && isHidden) {
                        showNav();
                        scrolled = true;
                    }else if (scroll < livePlayerBottom && !isHidden){
                        hideNav();
                        scrolled = false;
                    }
                });

                setTimeout(function () {
                    hideNav();
                }, 3000);

                $header.hover(function() {
                    if (isHidden && !scrolled) {
                        showNav();
                    }
                }, function() {
                    setTimeout(function () {
                        if (!isHidden && !scrolled) {
                            hideNav();
                        }
                    }, 500);
                });
            }
        }

        function animationsForm(){
            if ($(".input-field").length > 0) {
                $(".input-field input").click(function(){

                    $("input").each(function(){
                        if($(this).val().length == 0){
                            $(this).parent().find("label").removeClass("active");
                        }
                    });
                    if($(this).parent().find("label").hasClass("active") && $(this).val().length == 0){
                        $(this).parent().find("label").removeClass("active");
                    }else{
                        $(this).focus().parent().find("label").addClass("active");
                    }
                });
            }
        }

        function menuLeft(){
            $("[data-toggle='menuLeft']").click(function() {
                if($(".navbar-collapse").hasClass("showMenu")){
                    $(".navbar-collapse").removeClass("showMenu");
                    setTimeout(function(){
                        $(".navbar-collapse").removeClass("animationMenu");
                        $("html,body").removeClass("blockMenu");
                    },100);

                }else{
                    $("html,body").addClass("blockMenu");
                    $(".navbar-collapse").addClass("animationMenu");
                    setTimeout(function(){$(".navbar-collapse").addClass("showMenu");},10);
                }
            });
        }

        $(document).ready(function() {

            setTimeout(function () {

                initCarousel();
                countDown();
                destacadaHeader();
                navbarWithPlayer();
                animationsForm();
                menuLeft();
            }, 350);

        });
    });

})(jQuery);
