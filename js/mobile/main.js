jQuery.noConflict();
(function($) {
    function menuLeft() {
        $("[data-toggle='menuLeft']").click(function() {
            if ($(".navbar-collapse").hasClass("showMenu")) {
                $(".navbar-collapse").removeClass("showMenu");
                setTimeout(function() {
                    $(".navbar-collapse").removeClass("animationMenu");
                    $("html,body").removeClass("blockMenu");
                }, 100);

            } else {
                $("body").addClass("blockMenu");
                $(".navbar-collapse").addClass("animationMenu");
                setTimeout(function() {
                    $(".navbar-collapse").addClass("showMenu");
                }, 10);
            }
        });
    }

    function initSwiper() {

        if ($('.swiper-container').length > 0) {
            var swiper = new Swiper('.swiper-container.with-preview', {
                slidesPerView: 'auto',
                spaceBetween: 15
            });

            var swiperAboutUs = new Swiper('.about-us .swiper-container', {
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                },
            });
        }

    }

    function countDown() {
        if ($("#clock").length > 0) {
            $('#clock').countdown('2018/3/12', function(event) {
                var $this = $(this).html(event.strftime('' +
                    '<span class="float-left" ><em>%D</em><label>DIAS</lable></span>  ' +
                    '<span class="float-left" ><em>%H</em><label>HRS</label></span>  ' +
                    '<span class="float-left" ><em>%M</em><label>MIN</label></span>  '
                ));
            });
        }
    }
    function hideProgramTitle(){
        if($(".opening-live [data-headerfixed]").length > 0){
            setTimeout(function(){
                $(".opening-live [data-headerfixed]").fadeOut( "slow" );
            },3000);
        }
    }

    function grillaAnimation(){
        $("#grillaAnimation1").click(function(){
            var alto = 0;
            alto += $(".grilla span:first").outerHeight();
            alto += $(".grilla .live:first").outerHeight();
            alto += $(".grilla .separator:first").outerHeight();
            alto += $(".grilla .show:first").outerHeight();
            alto += $(".grilla .arrow").outerHeight();
            $(".grilla").css("transform","translateY(calc(100% - "+alto+"px");
        });

        $("#grillaAnimation2").click(function(){
            $(".grilla").css("transform","translateY(0px");
            $("#grillaAnimation2").remove();
        });

        $("#grillaAnimation3").click(function(){
            $(".grilla").removeClass("fixed");
            $("#grillaAnimation2").remove();
            $("#grillaAnimation3").remove();
        });
    }

    $(document).ready(function() {
        setTimeout(function() {
            menuLeft();
            initSwiper();
            countDown();
            hideProgramTitle();
            grillaAnimation();
        }, 350);
    });
})(jQuery);
